#include <iostream>
#include <string>
#include <fstream>
#include <cassert>
#include <map>
#include <algorithm>
#include <stdexcept>

#include "shape.hpp"
#include "application.hpp"
#include "shape_factory.hpp"
#include <unordered_map>
#include <functional>

using namespace Drawing;
using namespace std;

namespace FunctionExplained
{
    void foo(int a) {}

    void (*ptr_fun)(int) = &foo;

    std::function<void(int)> generic_fun;

    class Foo
    {
    public:
        void operator()(int a) {}
    };

    template <typename F>
    void use(F func)
    {
        func(7);
    }

    void demo()
    {
        use(&foo);

        Foo f;
        use(f);

        generic_fun = &foo;
        generic_fun = Foo();
        generic_fun = [](int a) {};
    }
}

namespace Details
{
    class ShapeCreator
    {
    public:
        virtual Shape* create_shape() = 0;
        virtual ~ShapeCreator() = default;
    };

    class ShapeFactory
    {
        std::unordered_map<string, std::shared_ptr<ShapeCreator>> shape_factory_;
    public:
        bool register_creator(std::string id, std::shared_ptr<ShapeCreator> creator)
        {
            return shape_factory_.insert(make_pair(id, creator)).second;
        }

        Shape* create(std::string id)
        {
            return shape_factory_.at(id)->create_shape();
        }
    };
}

//Shape* create_shape(const std::string& id)
//{
//	if (id == "Circle")
//		return new Circle();
//	else if (id == "Rectangle")
//		return new Rectangle();

//	throw std::runtime_error("Bad identifier");
//}

class GraphicsDocument : public Document
{
	vector<Shape*> shapes_;
	string title_;
public:
	virtual void open(const string& file_name)
	{
		title_ = file_name;

		ifstream fin(file_name.c_str());

		string type_identifier;

		cout << "Loading a file...\n";
		while(!fin.eof())
		{
			if (fin >> type_identifier)
			{
				cout << type_identifier << "\n";

                Shape* shp_ptr = ShapeFactory::instance().create_object(type_identifier);
				shp_ptr->read(fin);

				shapes_.push_back(shp_ptr);
			}
		}

		cout << "Loading finished." << endl;
	}

	virtual void save(const std::string& file_name)
	{
		ofstream fout(file_name.c_str());

        for(const auto& shape : shapes_)
            shape->write(fout);

		fout.close();

		title_ = file_name;
	}

	virtual string get_title() const
	{
		return title_;
	}

	void show()
	{
		cout << "\n\nDrawing:\n";

        for(const auto& shape : shapes_)
            shape->draw();
	}

	~GraphicsDocument()
	{
		for(std::vector<Shape*>::iterator it = shapes_.begin(); it != shapes_.end(); ++it)
			delete *it;
	}
};

class GraphicsApplication : public Application
{
public:
	virtual Document* create_document()
	{
		return new GraphicsDocument();
	}
};

int main()
{
	GraphicsApplication app;

    app.open_document("../drawing.txt");

    Document* ptr_doc = app.find_document("../drawing.txt");

	ptr_doc->show();

	ptr_doc->save("new_drawing.txt");
}
