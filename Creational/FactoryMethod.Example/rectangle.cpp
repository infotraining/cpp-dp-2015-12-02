#include "rectangle.hpp"
#include "shape_factory.hpp"

namespace
{
    using namespace Drawing;

    bool is_registered = ShapeFactory::instance()
                            .register_creator("Rectangle", []{ return new Rectangle();});
}
