#include <iostream>
#include <memory>
#include <cassert>
#include <typeinfo>

using namespace std;

class Engine
{
public:
    virtual void start() = 0;
    virtual void stop() = 0;

    std::unique_ptr<Engine> clone() const
    {
        auto temp = do_clone();
        assert(typeid(*this) == typeid(*temp.get()));

        return temp;
    }

    virtual ~Engine() = default;

protected:
    virtual std::unique_ptr<Engine> do_clone() const = 0;
};

class Diesel : public Engine
{
public:
    virtual void start() override
    {
        cout << "Diesel starts\n";
    }

    virtual void stop() override
    {
        cout << "Diesel stops\n";
    }
protected:
    std::unique_ptr<Engine> do_clone() const override
    {
        return std::unique_ptr<Engine>(new Diesel(*this));
    }
};

class TDI : public Diesel
{
public:
    virtual void start() override
    {
        cout << "TDI starts\n";
    }

    virtual void stop() override
    {
        cout << "TDI stops\n";
    }

protected:
    std::unique_ptr<Engine> do_clone() const override
    {
        return std::unique_ptr<Engine>(new TDI(*this));
    }
};

class Hybrid : public Engine
{
public:
    virtual void start() override
    {
        cout << "Hybrid starts\n";
    }

    virtual void stop() override
    {
        cout << "Hybrid stops\n";
    }

protected:
    std::unique_ptr<Engine> do_clone() const override
    {
        return std::unique_ptr<Engine>(new Hybrid(*this));
    }
};

class Car
{
    std::unique_ptr<Engine> engine_;
public:
    Car(std::unique_ptr<Engine> engine) : engine_{ move(engine) }
    {}

    Car(const Car& source) : engine_ { source.engine_->clone() }
    {
    }

    void drive(int km)
    {
        engine_->start();
        cout << "Driving " << km << " kms\n";
        engine_->stop();
    }
};

int main()
{
    Car c1{ std::unique_ptr<Engine>(new TDI()) };

    c1.drive(100);

    cout << "\n";

    Car c2 = c1;
    c2.drive(200);
}

