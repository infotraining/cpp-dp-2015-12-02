#ifndef FACTORY_HPP_
#define FACTORY_HPP_

#include <iostream>
#include <string>
#include <memory>

// "Product"
class Service
{
public:
    // product's interface
    virtual std::string run() const = 0;
    virtual ~Service() {}
};

// "ConcreteProductA"
class ConcreteServiceA : public Service
{
public:
    std::string run() const override
	{
        return std::string("ConcreteServiceA::run()");
	}
};

// "ConcreteProductB"
class ConcreteServiceB : public Service
{
public:
    std::string run() const override
	{
        return std::string("ConcreteServiceB::run()");
	}
};

class ConcreteServiceC : public Service
{
    // Service interface
public:
    std::__1::string run() const override
    {
        return std::string("ConcreteServiceC::run()");
    }
};

// "Creator"
class ServiceCreator
{
public:
    virtual std::unique_ptr<Service> create_service() = 0; // factory method
    virtual ~ServiceCreator() {}
};

// "ConcreteCreator"
class ConcreteCreatorA : public ServiceCreator
{
public:
    virtual std::unique_ptr<Service> create_service() override
	{
        return std::unique_ptr<Service>(new ConcreteServiceA());
	}
};

// "ConcreteCreator"
class ConcreteCreatorB : public ServiceCreator
{
public:
    std::unique_ptr<Service> create_service() override
	{
        return std::unique_ptr<Service>(new ConcreteServiceB());
	}
};

class ConcreteCreatorC : public ServiceCreator
{
    // ServiceCreator interface
public:
    std::unique_ptr<Service> create_service() override
    {
        return std::unique_ptr<Service>(new ConcreteServiceC());
    }
};

#endif /*FACTORY_HPP_*/
