#include <string>

#include "simple_singleton.hpp"
#include "meyers_singleton.hpp"
#include "phoenix_singleton.hpp"
#include "devices.hpp"

int main()
{
    std::cout << "Start main..." << std::endl;
	try
	{
		char c = 'a';
		KeyboardDevice::instance().on_key_pressed(c);
		c = '@';
		KeyboardDevice::instance().on_key_pressed(c);
	}
	catch(const std::exception& e)
	{
		std::cout << std::string("Exception: ") + e.what() << std::endl;
	}

    std::cout << "End of main..." << std::endl;
}
