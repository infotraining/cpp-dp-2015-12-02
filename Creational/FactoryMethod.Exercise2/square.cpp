#include "square.hpp"
#include "shape_factory.hpp"

namespace
{
    using namespace Drawing;

    bool is_registered =
            ShapeFactory::instance()
                .register_creator("Square", [] { return new SquareWithRect();});
}
