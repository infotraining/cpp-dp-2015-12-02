#ifndef SQUARE_HPP_
#define SQUARE_HPP_

#include "rectangle.hpp"

// TODO: Dodać klase Square
namespace Drawing
{
    class Square : public ShapeBase
    {
        int size_;
    public:
        Square(int x = 0, int y = 0, int size = 0) : ShapeBase(x, y), size_(size)
        {}

        void draw() const override
        {
            std::cout << "Drawing square at: " << point() << " with size: "
                      << size_ << std::endl;
        }

        void read(std::istream& in) override
        {
            Point pt;
            int size;

            in >> pt >> size;

            set_point(pt);
            size_ = size;
        }

        void write(std::ostream& out) override
        {
            out << "Square " << point() << " " << size_;
        }
    };

    class SquareWithRect : public Shape
    {
        std::unique_ptr<Rectangle> rect_;
    public:
        SquareWithRect(int x = 0, int y = 0, int size = 0) : rect_{new Rectangle(x, y, size, size)}
        {}

        void draw() const override
        {
            rect_->draw();
        }
        void move(int dx, int dy) override
        {
            rect_->move(dx, dy);
        }
        void read(std::istream& in) override
        {
            Point pt;
            int size;

            in >> pt >> size;

            rect_.reset(new Rectangle(pt.x(), pt.y(), size, size));
        }

        void write(std::ostream& out) override
        {
            out << "Square " << rect_->point() << " " << rect_->width();
        }
    };
}


#endif /* SQUARE_HPP_ */
