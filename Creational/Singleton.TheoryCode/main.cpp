#include <iostream>
#include "singleton.hpp"

using namespace std;

int main()
{
    cout << "Start main..." << endl;

	Singleton::instance().do_something();

	Singleton& singleObject = Singleton::instance();
	singleObject.do_something();

    Singleton* ptrObject =&Singleton::instance();
    ptrObject->do_something();
}
