#ifndef SINGLETON_HPP_
#define SINGLETON_HPP_

#include <iostream>
#include <mutex>
#include <atomic>

class Singleton
{
public:
    Singleton(const Singleton&) = delete;
    Singleton& operator=(const Singleton&) = delete;

	static Singleton& instance()
	{
        if (!instance_)
        {
            std::lock_guard<std::mutex> lk{mtx_};

            if (!instance_)
            {
                Singleton* temp = new Singleton();

                instance_ = temp; // operacja store na zmiennej atomowej - ochrona przed reorderingiem
            }
        }

        return *instance_;
	}

    void do_something();

private:
    static std::atomic<Singleton*> instance_;  // uniqueInstance
    static std::mutex mtx_;
	
	Singleton() // uniemozliwienie klientom tworzenie nowych singletonow
	{ 
		std::cout << "Constructor of singleton" << std::endl; 
	} 

	~Singleton() // prywatny destruktor chroni przed wywolaniem delete dla adresu instancji
	{ 
		std::cout << "Singleton has been destroyed!" << std::endl;
	} 
};

std::atomic<Singleton*> Singleton::instance_;
std::mutex Singleton::mtx_;

void Singleton::do_something()
{
	std::cout << "Singleton instance at " << std::hex << &instance() << std::endl;
}

#endif /*SINGLETON_HPP_*/
