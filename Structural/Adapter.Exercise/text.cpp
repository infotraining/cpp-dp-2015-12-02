#include "text.hpp"
#include "clone_factory.hpp"

namespace
{
    using namespace Drawing;

	// TODO - rejestracja prototypu Text
    bool is_registered
        = ShapeFactory::instance()
            .register_shape("Text", new Text());
}
