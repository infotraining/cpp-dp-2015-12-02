#ifndef SHAPE_GROUP_HPP_
#define SHAPE_GROUP_HPP_

#include "shape.hpp"
#include "clone_factory.hpp"
#include <vector>

namespace Drawing
{  
    using ShapePtr = std::shared_ptr<Shape>;

// TO DO: zaimplementowac kompozyt grupuj�cy kszta�ty geometryczne
class ShapeGroup : public Shape
{
    std::vector<ShapePtr> shapes_;

    // Shape interface
public:
    ShapeGroup() = default;

    ShapeGroup(const ShapeGroup& source)
    {
        for(const auto& s : source.shapes_)
            shapes_.emplace_back(s->clone());
    }

    ShapeGroup& operator=(const ShapeGroup& source)
    {
        if (this != &source)
        {
            std::vector<ShapePtr> temp;
            temp.reserve(source.shapes_.size());

            for(const auto& s : source.shapes_)
                temp.emplace_back(s->clone());

            shapes_.swap(temp);
        }

        return *this;
    }

    ShapeGroup(ShapeGroup&&) = default;
    ShapeGroup& operator=(ShapeGroup&&) = default;

    void draw() const override
    {
        for(const auto& s : shapes_)
            s->draw();
    }

    void move(int dx, int dy) override
    {
        for(const auto& s : shapes_)
            s->move(dx, dy);
    }

    void read(std::istream& in) override
    {
        int count;

        in >> count;

        for(int i = 0; i < count; ++i)
        {
            std::string type_identifier;

            in >> type_identifier;

            Shape* s = ShapeFactory::instance().create(type_identifier);
            s->read(in);

            shapes_.emplace_back(s);
        }
    }

    void write(std::ostream& out) override
    {
        out << "ShapeGroup " << shapes_.size() << std::endl;

        for(const auto& s : shapes_)
            s->write(out);
    }

    Shape*clone() const override
    {
        return new ShapeGroup(*this);
    }
};

}

#endif /*SHAPE_GROUP_HPP_*/
