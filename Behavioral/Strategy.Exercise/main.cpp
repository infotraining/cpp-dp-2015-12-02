#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <numeric>
#include <fstream>
#include <iterator>
#include <list>
#include <stdexcept>

struct StatResult
{
	std::string description;
	double value;

	StatResult(const std::string& desc, double val) : description(desc), value(val)
	{
	}
};

using Results = std::vector<StatResult>;

enum StatisticsType
{
	AVG, MINMAX, SUM
};

class Statistics
{
public:
    virtual void calculate(const std::vector<double>& data, Results& results) = 0;
    virtual ~Statistics() = default;
};

class Avg : public Statistics
{
public:
    void calculate(const std::vector<double>& data, Results& results) override
    {
        double sum = std::accumulate(data.begin(), data.end(), 0.0);
        double avg = sum / data.size();

        StatResult result("AVG", avg);
        results.push_back(result);
    }
};

class MinMax : public Statistics
{
public:
    void calculate(const std::vector<double>& data, Results& results) override
    {
        double min = *(std::min_element(data.begin(), data.end()));
        double max = *(std::max_element(data.begin(), data.end()));

        results.push_back(StatResult("MIN", min));
        results.push_back(StatResult("MAX", max));
    }
};

class Sum : public Statistics
{
public:
    void calculate(const std::vector<double>& data, Results& results) override
    {
        double sum = std::accumulate(data.begin(), data.end(), 0.0);

        results.push_back(StatResult("SUM", sum));
    }
};

using StatisticsPtr = std::shared_ptr<Statistics>;

class StatGroup : public Statistics
{
    std::vector<StatisticsPtr> stats_;
public:

    void add(StatisticsPtr stat)
    {
        stats_.push_back(stat);
    }

    void calculate(const std::vector<double>& data, Results& results) override
    {
        for(const auto& s : stats_)
            s->calculate(data, results);
    }
};

class DataAnalyzer
{
    StatisticsPtr statistics_;
	std::vector<double> data_;
	Results results_;
public:
    DataAnalyzer(StatisticsPtr statistics) : statistics_{statistics}
	{
	}

	void load_data(const std::string& file_name)
	{
		data_.clear();
		results_.clear();

		std::ifstream fin(file_name.c_str());
		if (!fin)
			throw std::runtime_error("File not opened");

		double d;
		while (fin >> d)
		{
			data_.push_back(d);
		}

		std::cout << "File " << file_name << " has been loaded...\n";
	}

    void set_statistics(StatisticsPtr statistics)
	{
        statistics_ = statistics;
	}

	void calculate()
	{
        statistics_->calculate(data_, results_);
	}

	const Results& results() const
	{
		return results_;
	}
};

void show_results(const Results& results)
{
    for(const auto& rslt : results)
		std::cout << rslt.description << " = " << rslt.value << std::endl;
}

int main()
{
    using namespace std;

    auto AVG = make_shared<Avg>();
    auto MINMAX = make_shared<MinMax>();
    auto SUM = make_shared<Sum>();

    auto STD_STAT = make_shared<StatGroup>();
    STD_STAT->add(AVG);
    STD_STAT->add(MINMAX);
    STD_STAT->add(SUM);

    DataAnalyzer da {STD_STAT};
	da.load_data("data.dat");
	da.calculate();

	show_results(da.results());

	std::cout << "\n\n";

	da.load_data("new_data.dat");
	da.calculate();

	show_results(da.results());
}
