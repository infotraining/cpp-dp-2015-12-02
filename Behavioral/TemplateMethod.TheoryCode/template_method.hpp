#ifndef TEMPLATE_METHOD_HPP_
#define TEMPLATE_METHOD_HPP_

#include <iostream>
#include <string>
#include  <memory>

class Service
{
public:
	virtual void run(const std::string& data) = 0;
	virtual ~Service() = default;
};

class BasicService : public Service
{
public:
	virtual void run(const std::string& data)
	{
		std::cout << "BasicService runs with " << data << std::endl;
	}
};

class AdvancedService : public Service
{
public:
	virtual void run(const std::string& data)
	{
		std::cout << "AdvancedService runs with " << data << std::endl;
	}
};

// "AbstractClass"
class AbstractClass
{
protected:
	virtual void primitive_operation_1() = 0;

	virtual void primitive_operation_2() = 0;

	virtual bool is_valid() const = 0;

	virtual std::unique_ptr<Service> create_service() const
	{
		return std::unique_ptr<Service>{ new BasicService() };
	}
public:
	void template_method()
	{
		primitive_operation_1();

		if (is_valid())
			primitive_operation_2();

		auto srv = create_service();
		srv->run("Hello from Template Method");
	}
	
	virtual ~AbstractClass() {}
};

// "ConcreteClass"
class ConcreteClassA : public AbstractClass
{
protected:
	void primitive_operation_1() override
	{
		std::cout << "ConcreteClassA.PrimitiveOperation1()" << std::endl;
	}

	void primitive_operation_2() override
	{
		std::cout << "ConcreteClassA.PrimitiveOperation2()" << std::endl;
	}

	bool is_valid() const override
	{
		return true;
	}
};

// "ConcreteClass"
class ConcreteClassB : public AbstractClass
{
protected:
	void primitive_operation_1() override
	{
		std::cout << "ConcreteClassB.PrimitiveOperation1()" << std::endl;
	}

	void primitive_operation_2() override
	{
		std::cout << "ConcreteClassB.PrimitiveOperation2()" << std::endl;
	}

	bool is_valid() const override
	{
		return false;
	}

	std::unique_ptr<Service> create_service() const override
	{
		return std::unique_ptr<Service>{ new AdvancedService() };
	}
};

#endif /*TEMPLATE_METHOD_HPP_*/